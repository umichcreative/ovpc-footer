<?php // OVPC FOOTER ?>
<footer id="ovpc-footer">
    <div class="row">

        <?php // TOP ?>
        <section id="footer-top">

            <?php // University of Michigan Logo ?>
            <div id="footer-logo" class="column">
                <a href="https://www.umich.edu/">
                    <img src="<?=get_stylesheet_directory_uri();?>/ovpc-footer/images/logo-umich.png" alt="University of Michigan Logo">
                </a>
            </div>

            <?php // Contact Information
                $widget_slug = 'ovpc-footer-contact-information';
                if ( is_active_sidebar( $widget_slug ) ) {
                    dynamic_sidebar( $widget_slug );
                }
            ?>

            <?php // Social Media
                $menu_location = 'ovpc-social-media';
                if( has_nav_menu( $menu_location ) ) {
                    // Get assigned menu ID
                    $menu_id = get_nav_menu_locations()[ $menu_location ];
                    // Get assigned menu items based off of menu ID
                    $menu_items = wp_get_nav_menu_items( $menu_id );
            ?>
                <ul id="footer-social-media">
                    <?php foreach( $menu_items as $menu_key => $menu_item ) { ?>
                        <li>
                            <a href="<?=$menu_item->url;?>">
                                <i class="fab fa-<?=$menu_item->post_name;?>"></i>
                                <span class="screen-reader-text hidden"><?=$menu_item->title;?></span>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            <?php } ?>

        </section>

        <?php // MENUS
            $ovpcMenus = array(
                'ovpc-footer-primary'   => 'footer-menu-primary',
                'ovpc-footer-secondary' => 'footer-menu-secondary'
            );
            foreach ($ovpcMenus as $ovpcMenu => $ovpcMenuID) {
                if( has_nav_menu( $ovpcMenu ) ) {
                    wp_nav_menu(array(
                        'theme_location' => $ovpcMenu,
                        'container'      => false,
                        'items_wrap'     => '<ul id="' . $ovpcMenuID . '" class="footer-menu">%3$s</ul>'
                    ));
                }
            }
        ?>

        <?php // BOTTOM ?>
        <section id="footer-bottom">
            <?php // Copyright ?>
            <span id="footer-copyright" class="column">
                &copy; <?=date('Y');?> <a href="http://regents.umich.edu">Regents of the University of Michigan</a>
            </span>
            <?php // Credit ?>
            <span id="footer-credit" class="column">
                Site by <a href="http://creative.umich.edu/">Michigan Creative</a>
            </span>
        </section>

    </div>
</footer>