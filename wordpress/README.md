# OVPC Footer for WordPress

## Files

### functions.php

Add this portion of code to the bottom of the active theme's **functions.php** file.

### /ovpc-footer

Simply add this folder into the active theme's folder.

## Dashboard

### Appearance > Widgets

The text for the contact information in the footer fall under the **Contact Information (OVPC Footer)** widget area.

1. Add a **Text** widget.
2. Add the division's name in the **Title:** field.
3. Add the divsion's contact information and phone number in the **editor**. For example:

```
<a href="https://www.google.com/maps/place/Michigan+Creative/@42.2776059,-83.7568467,17z/data=!4m12!1m6!3m5!1s0x883cae247cf03337:0x7179cad38238dce5!2sMichigan+Creative!8m2!3d42.277602!4d-83.754658!3m4!1s0x883cae247cf03337:0x7179cad38238dce5!8m2!3d42.277602!4d-83.754658">Argus I Building, 535 W. William St., Suite 2100
Ann Arbor, MI 48103</a>

<a href="tel:+1-734-764-9270">(734) 764-9270</a>
```

**Note:** The **Office of the Vice President for Global Communications** and **University of Michigan** links are already built in, so there is no need to add that information.

### Appearance > Menus

To add menus to the designated areas within the footer, you must create a menu and select a **Display location** under **Menu Settings**.

There are three locations:

1. **Social Media (OVPC Footer)**
- This area controls the social media icons to the right of the contact information.
- To create the icons, just simply type the name of the social media service (e.g. Facebook, Twitter, Youtube, LinkedIn, Instagram, Pinterest, etc.) in the menu item's **Navigation Label**. This will tell Font Awesome which icon to use.
2. **Primary Menu (OVPC Footer)**
- This is the white menu below the logo and contact information area.
3. **Secondary Menu (OVPC Footer)**
- This is the yellow menu below the primary menu and above the copyright information.

## Foundation Themes

If you are updating a theme that uses **foundation-skeleton** or **michigan-framework** as the parent theme, be sure to go into the active child theme's **config.json** file and disable all footer widgets.

If there are footer styles still appearing after disabling the widget areas, add styling overrides to the bottom of **ovpc-footer.css**.