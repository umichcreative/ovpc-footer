/************************************************/
/***************** OVPC FOOTER ******************/
/************************************************/
class ovpc_Footer
{
    /**
     * Register Actions
     **/
    static public function init()
    {
        // Enqueue Scripts
        add_action( 'wp_enqueue_scripts', 'ovpc_Footer::enqueue_scripts', 2 );
        // Setup Theme
        add_action( 'after_setup_theme', 'ovpc_Footer::setup_theme' );
        // Create Widget Areas
        add_action( 'widgets_init', 'ovpc_Footer::widget_areas' );
        // Add to wp_footer
        add_action( 'wp_footer', 'ovpc_Footer::add_ovpc_footer' ); 
    }

    /**
     * Enqueue Scripts
     **/
    static public function enqueue_scripts() 
    {
        // Register Styles
        wp_enqueue_style( 'ovpc-footer', get_stylesheet_directory_uri() . '/ovpc-footer/ovpc-footer.css' );
    }

    /**
     * Setup Theme
     **/
    static public function setup_theme()
    {
        // Register Menus
        register_nav_menus( array(
            'ovpc-social-media'     => __( 'Social Media (OVPC Footer)' ),
            'ovpc-footer-primary'   => __( 'Primary Menu (OVPC Footer)' ),
            'ovpc-footer-secondary' => __( 'Secondary Menu (OVPC Footer)' )
        ) );
    }

    /**
     * Create Widget Areas 
     **/
    static public function widget_areas() {
        // Register Widget
        register_sidebar( array(
            'name'          => 'Contact Information (OVPC Footer)',
            'id'            => 'ovpc-footer-contact-information',
            'before_widget' => '<div id="footer-information" class="column">',
            'after_widget'  => '</div>',
            'before_title'  => '<h6 id="division-name">',
            'after_title'   => '</h6><p><a href="http://vpcomm.umich.edu/">Office of the Vice President for Communications</a></p><p><a href="https://www.umich.edu/">University of Michigan</a></p>',
        ) );
    }

    /**
     * Add to wp_footer
     **/
    static public function add_ovpc_footer() {
        // Include OVPC Footer
        include get_stylesheet_directory() . '/ovpc-footer/ovpc-footer.php';
    }
}
ovpc_Footer::init();