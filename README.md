# OVPC Footer

This project is to replace all footers of websites associated with the University of Michigan's Office of the Vice President for Global Communications. If you are working on a static site, grab the **/static** directory. If you are working on a WordPress site, grab the **/wordpress** directory.

### Files
- **static/images/** contains all of the images used in the OVPC footer.
- **static/ovpc-footer.css** is the global stylesheet for **static/ovpc-footer.html**.
- **static/ovpc-footer.html** contains all of the necessary elements for the new OVPC footer.

### Demo
- http://esulliv.umwebops.org/ovpc-footer/

## WordPress

### Files
- **wordpress/functions.php** is to be added at the bottom of the active theme's functions.php file
- **wordpress/ovpc-footer/images/** contains all of the images used in the OVPC footer.
- **wordpress/ovpc-footer/ovpc-footer.css** is the global stylesheet for **wordpress/ovpc-footer/ovpc-footer.php**.
- **wordpress/ovpc-footer/ovpc-footer.php** contains all of the necessary elements for the new OVPC footer, with the dynamic areas of menus and widgets.

### Demo
- http://creative.umich.edu/